package client

import (
	"google.golang.org/grpc"

	"post_api_gateway/config"
	"post_api_gateway/genproto/post_grud_service"
	"post_api_gateway/genproto/post_service"
)

type ServiceManagerI interface {
	// PostGrud Service
	PostGrudService() post_grud_service.PostGrudServiceClient
	PostService() post_service.PostServiceClient
}

type grpcClients struct {
	post_grud_service post_grud_service.PostGrudServiceClient
	post_service      post_service.PostServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	connPostGrudService, err := grpc.Dial(
		cfg.PostsGrudServiceHost+cfg.PostsGrudGRPCPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	connPostService, err := grpc.Dial(
		cfg.PostsServiceHost+cfg.PostsGRPCPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		post_grud_service: post_grud_service.NewPostGrudServiceClient(connPostGrudService),
		post_service:      post_service.NewPostServiceClient(connPostService),
	}, nil
}

func (g *grpcClients) PostGrudService() post_grud_service.PostGrudServiceClient {
	return g.post_grud_service
}

func (g *grpcClients) PostService() post_service.PostServiceClient {
	return g.post_service
}
