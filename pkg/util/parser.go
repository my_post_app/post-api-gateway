package util

import (
	"encoding/json"
	"fmt"

	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/reflect/protoreflect"
)

func ProtoToJson(msg protoreflect.ProtoMessage, r interface{}) error {
	var (
		err   error
		bytes []byte
	)
	jsonMarshaller := protojson.MarshalOptions{
		UseEnumNumbers:  false,
		EmitUnpopulated: true,
	}
	bytes, err = jsonMarshaller.Marshal(msg)
	if err != nil {
		return err
	}
	err = json.Unmarshal(bytes, &r)
	if err != nil {
		return err
	}
	return nil
}

func JsonToProto(r interface{}, msg protoreflect.ProtoMessage) error {
	var (
		err   error
		bytes []byte
	)
	jsonUnMarshaller := protojson.UnmarshalOptions{
		AllowPartial: true,
	}

	bytes, err = json.Marshal(&r)
	if err != nil {
		return err
	}
	err = jsonUnMarshaller.Unmarshal(bytes, msg)
	if err != nil {
		return err
	}

	return nil
}

func ProtoToProto(msg1, msg2 protoreflect.ProtoMessage) error {
	var (
		err   error
		bytes []byte
	)
	jsonUnMarshaller := protojson.UnmarshalOptions{
		AllowPartial: true,
	}

	jsonMarshaller := protojson.MarshalOptions{
		UseEnumNumbers:  false,
		EmitUnpopulated: true,
	}

	bytes, err = jsonMarshaller.Marshal(msg1)
	if err != nil {
		return err
	}

	err = jsonUnMarshaller.Unmarshal(bytes, msg2)
	if err != nil {
		return err
	}
	fmt.Print(msg2)

	return nil
}
