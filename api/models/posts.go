package models

type PlaceOrderResponse struct {
	UserId     string `json:"user_id"`
	SymbolId   string `json:"symbol_id"`
	Side       string `json:"side"`
	Quantity   string `json:"quantity"`
	LimitPrice string `json:"limit_price"`
}

type ModifyOrderRequest struct {
	Action     string `json:"action"`
	LimitPrice string `json:"limit_price,omitempty"`
	OrderId    string `json:"order_id,omitempty"`
	Quantity   string `json:"quantity,omitempty"`
}

type UpdatePost struct {
	Title  string `json:"title"`
	Body   string `json:"body"`
	UserId int32  `json:"user_id"`
}
