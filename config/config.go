package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

const (
	// DebugMode indicates service mode is debug.
	DebugMode = "debug"
	// TestMode indicates service mode is test.
	TestMode = "test"
	// ReleaseMode indicates service mode is release.
	ReleaseMode = "release"
)

type Config struct {
	ServiceName string
	Environment string // debug, test, release
	Version     string

	HTTPPort   string
	HTTPScheme string

	DefaultOffset string
	DefaultLimit  string

	// Post Grud Service
	PostsGrudServiceHost string
	PostsGrudGRPCPort    string

	// Post Service
	PostsServiceHost string
	PostsGRPCPort    string
}

// Load ...
func Load() Config {
	if err := godotenv.Load("/app/.env"); err != nil {
		if err := godotenv.Load("./.env"); err != nil {
			fmt.Println("No .env file found")
		}
	}

	config := Config{}

	config.ServiceName = cast.ToString(getOrReturnDefaultValue("SERVICE_NAME", "post_api_gateway"))
	config.Environment = cast.ToString(getOrReturnDefaultValue("ENVIRONMENT", DebugMode))
	config.Version = cast.ToString(getOrReturnDefaultValue("VERSION", "1.0"))

	config.HTTPPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":8080"))
	config.HTTPScheme = cast.ToString(getOrReturnDefaultValue("HTTP_SCHEME", "http"))

	config.DefaultOffset = cast.ToString(getOrReturnDefaultValue("DEFAULT_OFFSET", "0"))
	config.DefaultLimit = cast.ToString(getOrReturnDefaultValue("DEFAULT_LIMIT", "10"))

	// Order Service
	config.PostsGrudServiceHost = cast.ToString(getOrReturnDefaultValue("POST_GRUD_SERVICE_HOST", "localhost"))
	config.PostsGrudGRPCPort = cast.ToString(getOrReturnDefaultValue("POST_GRUD_SERVICE_PORT", ":7002"))

	// Post Service
	config.PostsServiceHost = cast.ToString(getOrReturnDefaultValue("POST_SERVICE_HOST", "localhost"))
	config.PostsGRPCPort = cast.ToString(getOrReturnDefaultValue("POST_GRPC_PORT", ":7001"))

	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}

	return defaultValue
}
