CURRENT_DIR=$(shell pwd)

APP=$(shell basename ${CURRENT_DIR})
APP_CMD_DIR=${CURRENT_DIR}/cmd

TAG=latest
ENV_TAG=latest

gen-proto-module:
	./scripts/gen_proto.sh ${CURRENT_DIR}
	./scripts/remove-omitempty.sh ${CURRENT_DIR}

migration-up:
	migrate -path ./migrations/postgres -database 'postgres://postgres:admin1234@0.0.0.0:5432/post_api_gateway?sslmode=disable' up

migration-down:
	migrate -path ./migrations/postgres -database 'postgres://postgres:admin1234@0.0.0.0:5432/post_api_gateway?sslmode=disable' down


swag-init:
	swag init -g api/api.go -o api/docs

run:
	go run cmd/main.go
