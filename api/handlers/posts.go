package handlers

import (
	"post_api_gateway/api/http"
	"post_api_gateway/api/models"
	"post_api_gateway/genproto/post_grud_service"
	"post_api_gateway/genproto/post_service"
	"strconv"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/types/known/emptypb"
)

// CreatePosts godoc
// @ID CreatePosts
// @Router /post [POST]
// @Summary Create posts
// @Description Create posts
// @Tags Posts
// @Accept json
// @Produce json
// @Success 200 {object} http.Response{data=post_service.CreatePostResponse} "CreatePostResponse"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreatePosts(c *gin.Context) {
	var (
		resp *post_service.CreatePostResponse
		err  error
	)

	resp, err = h.services.PostService().CreatePost(
		c.Request.Context(),
		&emptypb.Empty{},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetPosts godoc
// @ID GetPosts
// @Router /post [GET]
// @Summary Get all posts
// @Description Get all posts
// @Tags Posts
// @Accept json
// @Produce json
// @Success 200 {object} http.Response{data=post_grud_service.GetPostsResponse} "GetAllPostResponse"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetPosts(c *gin.Context) {
	var (
		resp *post_grud_service.GetPostsResponse
		err  error
	)

	resp, err = h.services.PostGrudService().GetPosts(
		c.Request.Context(),
		&emptypb.Empty{},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetPostById godoc
// @ID GetPostById
// @Router /post/{id} [GET]
// @Summary GetPostById
// @Description Get  post by id
// @Tags Posts
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=post_grud_service.Post} "GetPostResponse"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetPostByID(c *gin.Context) {
	var (
		resp *post_grud_service.Post
		err  error
	)

	resp, err = h.services.PostGrudService().GetPostByID(
		c.Request.Context(),
		&post_grud_service.IdReq{Id: c.Param("id")},
	)
	if err != nil {
		if err.Error() == "rpc error: code = Unknown desc = no rows in result set" {
			h.handleResponse(c, http.NOT_FOUND, err.Error())
			return
		}
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeletePost godoc
// @ID DeletePost
// @Router /post/{id} [DELETE]
// @Summary DeletePost
// @Description delete post by id
// @Tags Posts
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=post_grud_service.Response} "GetAllPostResponse"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeletePost(c *gin.Context) {
	var (
		resp *post_grud_service.Response
		err  error
	)

	resp, err = h.services.PostGrudService().DeletePost(
		c.Request.Context(),
		&post_grud_service.IdReq{Id: c.Param("id")},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdatePost godoc
// @ID UpdatePost
// @Router /post/{id} [PUT]
// @Summary UpdatePost
// @Description delete post by id
// @Tags Posts
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param data body models.UpdatePost true "data"
// @Success 200 {object} http.Response{data=post_grud_service.Response} "GetAllPostResponse"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePost(c *gin.Context) {
	var (
		resp  *post_grud_service.Response
		model models.UpdatePost
		err   error
	)
	err = c.ShouldBindJSON(&model)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		// ... handle error
		panic(err)
	}

	resp, err = h.services.PostGrudService().UpdatePost(
		c.Request.Context(),
		&post_grud_service.Post{
			Id:     int32(id),
			Body:   model.Body,
			Title:  model.Title,
			UserId: model.UserId,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}
